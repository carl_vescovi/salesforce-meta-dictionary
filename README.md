# README #


### What is this repository for? ###

* A single page utility that once installed to an Org will prompt for Salesforce sobjects to retrieve, including fields and field descriptions.
  fields can then be exported to a simple HTML table, which in turn can be copied into Excel or whatever other format you have in mind.

* Written to avoid having to generate those awful data dictionarys manually! 

* Version 0.1


### Why write this ? ###

* Salesforce make it difficult to readily extract helpful description metadata when building a picture of an org's Schema.  This tries to help a little bit - it certainly helped me, so I figured why not share!

### How do I get set up? ###

* Install, or copy and paste the page into the Salesforce Org you want to use it in.

* go to the data dictionary page i.e. /apex/dd or /apex/<whatever you called the page>

* You will be prompted to select one or more sObjects to retrieve metadata for. Select one or more, then hit the 'generate' button. 

![dd1.png](https://bitbucket.org/repo/qrrMpx/images/3253624233-dd1.png)

* Wait for the magic...

* Voila! Tables built for your pleasure...

![dd2.png](https://bitbucket.org/repo/qrrMpx/images/1556901178-dd2.png)

* Now use the next option offered if you wish to export the information to a plain html page. From there you can copy and paste to Excel 
* or other applications.

![dd3.png](https://bitbucket.org/repo/qrrMpx/images/1383057743-dd3.png)

### Oh yeah.. ###

* I've only presented a couple of metadata options as they were all I needed. Feel free to extend this to include text field lengths etc. as desired.

### 21 Dec 16 update ###

* You'll find the jsZip upgrade to v3.0 breaks the page - use v2.5 still available at time of writing at https://github.com/cdnjs/cdnjs/tree/master/ajax/libs/jszip/2.5.0

